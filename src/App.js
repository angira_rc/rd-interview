import { useEffect, useState } from "react"
import { message } from "antd"
import axios from "axios"

import 'antd/dist/antd.css'

const handleError = err => {
    let msg = err.message

    if (err?.response?.data)
        msg = err.response.data.Message

    message.error(msg)
}

const Form = () => {
    const [names, setNames] = useState()
    const [email, setEmail] = useState()
    const [phone, setPhone] = useState()
    const [address, setAddess] = useState()

    const change = e => {
        switch (e.target.name) {
            case 'full names':
                setNames(e.target.value)
                break

            case 'email':
                setEmail(e.target.value)
                break

            case 'phone':
                setPhone(e.target.value)
                break

            case 'address':
                setAddess(e.target.value)
                break

            default:
                break
        }
    }

    const submit = async e => {
        e.preventDefault()

        if (!email.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)) {
            message.warning('Please check email format')
            return
        }

        try {
            let response = await axios({
                url: "http://developers.gictsystems.com/api/dummy/submit",
                method: 'POST',
                body: {
                    "full names": names,
                    email,
                    phone,
                    address
                }
            })
            let msg = response.message
            if (response.data)
                msg = response.data.Message

            message.success(msg)
        } catch (err) {
            handleError(err)
        }
    }

    return (
        <form onSubmit={ submit } method="post" action="http://developers.gictsystems.com/api/dummy/submit">
            <span>
                <label>Full Names</label>
                <input onChange={ change } name="full names" type="text" placeholder="Wilson Kamau" />
            </span>
            <span>
                <label>Email</label>
                <input onChange={ change } required name="email" type="email" placeholder="person@mail.com" />
            </span>
            <span>
                <label>Phone</label>
                <input onChange={ change } required name="phone" type="text" placeholder="07XXXXXXXX" />
            </span>
            <span>
                <label>Address</label>
                <input onChange={ change } required name="address" type="text" placeholder="1 Address" />
            </span>
            <button type="submit">Submit</button>
        </form>
    )
}

const Table = () => {
    let auth = "ALDJAK23423JKSLAJAF23423J23SAD3"

    const [data, setData] = useState([])
    const [dependency] = useState()

    const fetchData = async () => {
        try {
            let response = await axios({
                url: "http://developers.gictsystems.com/api/dummy/items",
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${auth}`
                }
            })
            setData(response.data)
            setTimeout(fetchData, 10000)
        } catch (err) {
            handleError(err)
        }
    }

    useEffect(() => {
        fetchData()
    }, [dependency])

    return (
        <div className="table-container">
            {
                data.length > 0 &&
                <table>
                    <thead>
                        <tr>
                            {
                                Object.keys(data[0]).map(title => 
                                    <th>{ title }</th>
                                )
                            }
                        </tr>
                    </thead>
                    <tbody>
                        {
                            data.map(item => 
                                <tr>
                                    {
                                        Object.keys(item).map(title => 
                                            <td>{ item[title] }</td>
                                        )
                                    }
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            }
        </div>
    )
}

const App = () => {
    const [page, setPage] = useState('form')

    return (
        <div className="body">
            <header>
                <a>Contact Sales</a>
                <a>Support</a>
                <a>Language {'>'} EN</a>
            </header>
            <nav>
                <div className="brand">Rich Dynamics</div>
                <div className="nav-tabs">
                    <a>Platform</a>
                    <a>Solutions</a>
                    <a>Developers</a>
                    <a>Why Rich Dynamics</a>
                    <a>Pricing</a>
                </div>
            </nav>
            <div>
                <div className="tabs">
                    <button className={page === 'form' ? 'active' : ''} onClick={() => setPage('form')}>Form</button>
                    <button className={page === 'table' ? 'active' : ''} onClick={() => setPage('table')}>Table</button>
                </div>
                {
                    page === 'form' ?
                        <Form />
                        : <Table />
                }
            </div>
            <footer>
                <p>Copyright © 2022 Rich Dynamics. All rights reserved.</p>
                <div className="nav-tabs">
                    <a>Privacy Policy</a>
                    <a>Terms of Use</a>
                    <a>Careers</a>
                    <a>Sitemap</a>
                </div>
            </footer>
        </div>
    )
}

export default App